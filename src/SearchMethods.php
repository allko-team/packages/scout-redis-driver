<?php


namespace Allko\RedisScoutEngine;


class SearchMethods
{
    const STRPOS = 'strpos';
    const STRIPOS = 'stripos';
    const WILDCARD = 'wildcard';
    const REGEX = 'regex';
}
